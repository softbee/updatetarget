package ch.softbee.updatetarget;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

abstract class SafeJob extends Job {

	private final IExceptionHandler handler;

	SafeJob(final IExceptionHandler handler) {
		super("Update Target");
		this.handler = handler;
		setUser(true);
	}

	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		try {
			safeRun(monitor, handler);
		} catch (final Exception e) {
			handler.handle(e);
		}
		return Status.OK_STATUS;
	}

	protected abstract void safeRun(IProgressMonitor monitor, IExceptionHandler handler) throws Exception;
}