package ch.softbee.updatetarget;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.pde.core.target.ITargetDefinition;
import org.eclipse.pde.core.target.ITargetLocation;
import org.eclipse.pde.core.target.ITargetPlatformService;
import org.eclipse.pde.core.target.LoadTargetDefinitionJob;
import org.eclipse.pde.internal.core.target.P2TargetUtils;
import org.eclipse.pde.internal.core.target.TargetPlatformService;
import org.eclipse.pde.internal.ui.shared.target.UpdateTargetJob;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

public class UpdateTargetHandler extends AbstractHandler {
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		try {
			safeExecute(window);
		} catch (final CoreException e) {
			openError(window, e);
		}

		return null;
	}

	private void safeExecute(final IWorkbenchWindow window) throws CoreException {
		final ITargetDefinition target = copyCurrentTarget();

		final IJobChangeListener listener = new SafeListener(window) {
			protected void done() throws CoreException {
				new UpdateTargetHandler.SafeJob("Updating Target", window) {
					protected void safeRun(final IProgressMonitor monitor) throws Exception {
						resolveAndLoad(target, monitor);
					}
				}.schedule();
			}
		};
		new SafeJob("Updating Target ", window) {
			protected void safeRun(final IProgressMonitor monitor) throws Exception {
				resolveAndUpdate(target, listener, monitor);
			}
		}.schedule();
	}

	private ITargetDefinition resolveAndUpdate(final ITargetDefinition target, final IJobChangeListener listener,
			final IProgressMonitor monitor) throws CoreException {
		target.resolve(monitor);
		final Map<ITargetLocation, Set<Object>> toUpdate = getTargetLocations(target);
		UpdateTargetJob.update(target, toUpdate, listener);
		return target;
	}

	private ITargetDefinition copyCurrentTarget() throws CoreException {
		final ITargetPlatformService service = TargetPlatformService.getDefault();
		final ITargetDefinition target = service.getWorkspaceTargetHandle().getTargetDefinition();
		final ITargetDefinition copy = target.getHandle().getTargetDefinition();
		service.copyTargetDefinition(target, copy);
		return copy;
	}

	static void resolveAndLoad(final ITargetDefinition activeTarget, final IProgressMonitor monitor)
			throws CoreException {
		activeTarget.resolve(monitor);
		TargetPlatformService.getDefault().saveTargetDefinition(activeTarget);
		LoadTargetDefinitionJob.load(activeTarget);
		P2TargetUtils.garbageCollect();
	}

	private Map<ITargetLocation, Set<Object>> getTargetLocations(final ITargetDefinition activeTarget)
			throws CoreException {
		final ITargetLocation[] containers = activeTarget.getTargetLocations();
		final Map<ITargetLocation, Set<Object>> toUpdate = new HashMap<ITargetLocation, Set<Object>>();
		for (final ITargetLocation container : containers) {
			toUpdate.put(container, new HashSet<Object>(0));
		}
		return toUpdate;
	}

	static void openError(final IWorkbenchWindow window, final Exception e) {
		MessageDialog.openError(window.getShell(), "Update Target", e.getMessage());
	}

	static abstract class SafeJob extends Job {
		private final IWorkbenchWindow window;

		SafeJob(final String name, final IWorkbenchWindow window) {
			super(name);
			setUser(true);
			this.window = window;
		}

		protected IStatus run(final IProgressMonitor monitor) {
			try {
				safeRun(monitor);
			} catch (final Exception e) {
				window.getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						openError(window, e);
					}
				});
			}
			return Status.OK_STATUS;
		}

		protected abstract void safeRun(IProgressMonitor paramIProgressMonitor) throws Exception;
	}

	private abstract class SafeListener extends JobChangeAdapter {
		private final IWorkbenchWindow window;

		private SafeListener(final IWorkbenchWindow window) {
			this.window = window;
		}

		public final void done(final IJobChangeEvent event) {
			try {
				done();
			} catch (final CoreException e) {
				window.getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						openError(window, e);
					}
				});
			}
		}

		protected abstract void done() throws CoreException;
	}

	static void load(final ITargetDefinition target, final ExceptionHandler handler) {

		new SafeJob("Load Target", PlatformUI.getWorkbench().getActiveWorkbenchWindow()) {

			@Override
			protected void safeRun(final IProgressMonitor monitor) throws Exception {
				resolveAndLoad(target, monitor);
			}
		}.schedule();

	}
}