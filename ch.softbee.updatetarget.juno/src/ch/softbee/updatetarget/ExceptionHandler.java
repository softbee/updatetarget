package ch.softbee.updatetarget;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.ui.handlers.HandlerUtil;

public class ExceptionHandler implements IExceptionHandler{

	private IShellProvider shellProvider;

	
	 ExceptionHandler(ExecutionEvent event) {
		shellProvider = HandlerUtil.getActiveWorkbenchWindow(event);
	}
	
	ExceptionHandler(final IShellProvider shellProvider) {
		this.shellProvider = shellProvider;
	}

	@Override
	public void handle(final Exception e) {
		shellProvider.getShell().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				MessageDialog.openError(shellProvider.getShell(), "Update Target", e.getMessage());
			}
		});
		throw new RuntimeException(e);
	}

}
