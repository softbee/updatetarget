package ch.softbee.updatetarget;

import static ch.softbee.updatetarget.UpdateTargetHandler.load;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.pde.core.target.ITargetHandle;
import org.eclipse.pde.core.target.ITargetPlatformService;
import org.eclipse.pde.internal.core.target.TargetPlatformService;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.menus.UIElement;

public class SetTargetHandler extends AbstractHandler implements IElementUpdater {


	public Object execute(final ExecutionEvent event) throws ExecutionException {

		try {
			setTarget(event);
		} catch (final CoreException e) {
			throw new RuntimeException(e);
		}

		return null;
	}

	private void setTarget(final ExecutionEvent event) throws CoreException, ExecutionException {
		final String name = event.getParameter("ch.softbee.updatetarget.commandParameter.target");

		final ITargetPlatformService service = TargetPlatformService.getDefault();
		final ITargetHandle target = getTarget(name, service);

		if (target != null) {
			load(target.getTargetDefinition(), new ExceptionHandler(event));
			refreshElements(event, name);
		}

	}

	private ITargetHandle getTarget(final String name, final ITargetPlatformService service) throws CoreException {
		for (final ITargetHandle handle : service.getTargets(new NullProgressMonitor())) {
			if (handle.toString().equals(name)) {
				return handle;
			}
		}
		return null;
	}

	@Override
	public void updateElement(final UIElement element, @SuppressWarnings("rawtypes") final Map parameters) {
		String name = (String) parameters.get("ch.softbee.updatetarget.commandParameter.target");
		System.err.println(name);
		element.setChecked(true);
	}

	private void refreshElements(final ExecutionEvent event, String name) throws ExecutionException {
		final ICommandService service = (ICommandService) HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getService(ICommandService.class);
		service.refreshElements(event.getCommand().getId(), Collections.singletonMap("ch.softbee.updatetarget.commandParameter.target", "f�dle"));
	}

}
