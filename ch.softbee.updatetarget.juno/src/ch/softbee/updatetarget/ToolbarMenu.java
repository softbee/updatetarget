package ch.softbee.updatetarget;

import java.util.Collections;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.pde.core.target.ITargetHandle;
import org.eclipse.pde.core.target.ITargetPlatformService;
import org.eclipse.pde.internal.core.target.TargetPlatformService;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.ExtensionContributionFactory;
import org.eclipse.ui.menus.IContributionRoot;
import org.eclipse.ui.services.IServiceLocator;

public class ToolbarMenu extends ExtensionContributionFactory {

	@Override
	public void createContributionItems(final IServiceLocator serviceLocator, final IContributionRoot additions) {

		try {
			safeExec(serviceLocator, additions);
		} catch (final CoreException e) {
			new ExceptionHandler((IWorkbenchWindow) serviceLocator.getService(IWorkbenchWindow.class)).handle(e);
		}

	}

	private void safeExec(final IServiceLocator serviceLocator, final IContributionRoot additions) throws CoreException {

		final IContributionItem item1 = createAddTarget(serviceLocator);
		additions.addContributionItem(item1, null);

		final IContributionItem item2 = createSetTarget(serviceLocator);
		additions.addContributionItem(item2, null);

		final IContributionItem item3 = createEditTarget(serviceLocator);
		additions.addContributionItem(item3, null);

	}

	private IContributionItem createEditTarget(final IServiceLocator serviceLocator) {
		CommandContributionItemParameter p = new CommandContributionItemParameter(serviceLocator, "",
				"org.eclipse.ui.window.preferences", SWT.PUSH);
		p.parameters = Collections.singletonMap("preferencePageId", "org.eclipse.pde.ui.TargetPlatformPreferencePage");
		final IContributionItem i = new CommandContributionItem(p);
		i.setVisible(true);
		return i;
	}

	private IContributionItem createAddTarget(final IServiceLocator serviceLocator) {
		final CommandContributionItemParameter p1 = new CommandContributionItemParameter(serviceLocator, "",
				"ch.softbee.updatetarget.commands.addTargetCommand", SWT.PUSH);

		final IContributionItem item = new CommandContributionItem(p1);
		item.setVisible(true);
		return item;
	}

	private IContributionItem createSetTarget(final IServiceLocator serviceLocator) {
		final CompoundContributionItem item2 = new CompoundContributionItem() {

			@Override
			protected IContributionItem[] getContributionItems() {

				final ITargetPlatformService service = TargetPlatformService.getDefault();

				ITargetHandle active;
				try {
					active = service.getWorkspaceTargetHandle();
				} catch (final CoreException e) {
					throw new RuntimeException(e);
				}

				final ITargetHandle[] targets = service.getTargets(new NullProgressMonitor());
				final IContributionItem[] items = new IContributionItem[targets.length];
				for (int i = 0; i < targets.length; i++) {

					final ITargetHandle handle = targets[i];
					items[i] = createSetTarget(serviceLocator, handle, active.equals(handle));

				}
				return items;
			}

		};
		return item2;
	}

	private IContributionItem createSetTarget(final IServiceLocator serviceLocator, final ITargetHandle handle,
			final boolean active) {
		final CommandContributionItemParameter p = new CommandContributionItemParameter(serviceLocator, "",
				"ch.softbee.updatetarget.commands.setTargetCommand", SWT.PUSH);

		String name;
		try {
			name = handle.getTargetDefinition().getName();
		} catch (final CoreException e) {
			throw new RuntimeException(e);
		}
		p.parameters = Collections.singletonMap("ch.softbee.updatetarget.commandParameter.target", handle.toString());
		p.label = name + (active ? " (Active)" : "");
		p.style = CommandContributionItem.STYLE_RADIO;
		
		

		final CommandContributionItem item = new CommandContributionItem(p) {
			@Override
			public boolean isEnabled() {
				return !active;
			}
			

		};
		item.setVisible(true);
		
		

		return item;
	}

}
