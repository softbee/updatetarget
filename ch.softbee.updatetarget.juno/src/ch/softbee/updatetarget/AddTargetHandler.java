package ch.softbee.updatetarget;

import static ch.softbee.updatetarget.UpdateTargetHandler.load;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.pde.core.target.ITargetDefinition;
import org.eclipse.pde.internal.core.target.TargetPlatformService;
import org.eclipse.pde.internal.ui.shared.target.InstallableUnitWizard;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;


public class AddTargetHandler extends AbstractHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {

		final ITargetDefinition newTarget = TargetPlatformService.getDefault().newTarget();

		final InstallableUnitWizard wizard = new InstallableUnitWizard();
		wizard.setTarget(newTarget);

		final Shell parent = HandlerUtil.getActiveShellChecked(event);
		final WizardDialog dialog = new WizardDialog(parent, wizard);
		if (dialog.open() != Window.OK) {
			return null;
		}

		final InputDialog inputDialog = new InputDialog(HandlerUtil.getActiveShellChecked(event), "Target Title",
				"Enter Name of Target Definition:", "New Target", null);
		if (inputDialog.open() != Window.OK) {
			return null;
		}

		
		newTarget.setTargetLocations(wizard.getLocations());
		newTarget.setName(inputDialog.getValue());

		load(newTarget, new ExceptionHandler(event));

		return null;
	}

}
