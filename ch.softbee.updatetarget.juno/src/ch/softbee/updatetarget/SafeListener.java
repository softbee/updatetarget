package ch.softbee.updatetarget;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;

abstract class SafeListener extends JobChangeAdapter {
	private final IExceptionHandler handler;

	SafeListener(final IExceptionHandler handler) {
		this.handler = handler;
	}

	@Override
	public final void done(final IJobChangeEvent event) {
		try {
			done(handler);
		} catch (final Exception e) {
			handler.handle(e);
		}
	}

	protected abstract void done(IExceptionHandler handler) throws Exception;
}