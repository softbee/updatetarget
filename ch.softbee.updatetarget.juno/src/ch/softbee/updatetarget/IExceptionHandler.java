package ch.softbee.updatetarget;

public interface IExceptionHandler {

	void handle(Exception e);

}
